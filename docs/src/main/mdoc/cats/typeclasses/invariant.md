---
id: invariant
title: Invariant
---

_TBD_

## Definition

```scala
trait Invariant[F[_]] {
  def imap[A, B](fa: F[A])(f: A ⇒ B)(g: B => A): F[B]
}
```

## Methods

### `imap`

```scala
def imap[F[_], A, B](fa: F[A])(f: A => B)(g: B => A): F[B]
```

Usage:

```scala mdoc:reset
import cats.Invariant
import cats.Semigroup
import cats.instances.invariant._
import cats.instances.long._
import scala.concurrent.duration._

val sgi: Semigroup[FiniteDuration] = 
  Invariant[Semigroup].imap(Semigroup[Long])(Duration.fromNanos)(_.toNanos)

sgi.combine(3 seconds, 5 seconds)
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#invariant-invariant-functor-exponential-functor