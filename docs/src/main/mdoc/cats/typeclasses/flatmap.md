---
id: flatmap
title: FlatMap
---

_TBD_

## Definition

```scala
import cats.Apply

trait FlatMap[F[_]] extends Apply[F] {
  def flatMap[A, B](fa: F[A])(f: A ⇒ F[B]): F[B]
}
```

## Methods

### `flatMap`

### `flatten`

### `mproduct`

### `tailRecM`

### `flatTap`