---
id: monoidK
title: MonoidK
---

`Monoid` that operates on kinds.

## Definition

```scala
import cats.SemigroupK

trait MonoidK[F[_]] extends SemigroupK[F] {
  def empty[A]: F[A]
}
```

## Methods

### `empty`

```scala
def empty[F[_], A]: F[A]
```

Usage:

```scala mdoc:reset
import cats.MonoidK
import cats.instances.list._
import cats.instances.option._

MonoidK[List].empty[Int]
MonoidK[Option].empty[String]
```

### `algebra`

Creates concrete `Monoid[F[A]]`

```scala
import cats.Monoid

def algebra[F[_], A]: Monoid[F[A]]
```

Usage:

```scala mdoc:reset
import cats.MonoidK
import cats.instances.list._

MonoidK[List].algebra[Int].combine(List(1), List(2))
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#monoidk-plusempty