---
id: semigroupK
title: SemigroupK
---

`Semigroup` that operates on kinds. Allows combining type constructors that can be combined for any particular type.

## Definition 

```scala
trait SemigroupK[F[_]] {
  def combineK[A](x: F[A], y: F[A]): F[A]
}
```

## Methods

### `combineK`

Combine two structures.

```scala
def combineK[F[_], A](x: F[A], y: F[A]): F[A]
```

Usage:

```scala mdoc:reset
import cats.SemigroupK
import cats.instances.list._
import cats.instances.option._

SemigroupK[List].combineK(List(1, 2), List(3, 4))

// using syntax
import cats.syntax.semigroupk._

List(1, 2) <+> List(3, 4)
None <+> Option("hello") <+> Option("world")
```

### `algebra`

Produce `Semigroup[F[A]]` for any type `A`.

```scala
import cats.Semigroup

def algebra[F[_], A]: Semigroup[F[A]]
```

Usage:

```scala mdoc:reset
import cats.SemigroupK
import cats.Semigroup
import cats.instances.list._
import cats.instances.option._

SemigroupK[List].algebra[Int].combine(List(1), List(2))
SemigroupK[Option].algebra[String].combine(Option("hello"), Option("hi"))
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#semigroupk-plus