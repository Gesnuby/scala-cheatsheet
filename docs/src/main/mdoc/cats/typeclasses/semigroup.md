---
id: semigroup
title: Semigroup
---

Allows combining two things of the same type with associative operation.

## Definition 

```scala
trait Semigroup[A] {
  def combine(x: A, y: A): A
}
```

## Methods

### `combine`

Combine two values with associative operation.

```scala
def combine[A](x: A, y: A): A
```

Usage:

```scala mdoc:reset
import cats.Semigroup
import cats.instances.int._
import cats.instances.string._
import cats.instances.list._
import cats.instances.option._

Semigroup[List[Int]].combine(List(1, 2), List(3, 4))

// using syntax
import cats.syntax.semigroup._
import cats.syntax.either._
import cats.instances.either._

List(1, 2) |+| List(3, 4)
Option("hello") |+| Option(" ") |+| Option("world")
5.asRight[String] |+| 10.asRight[String]
"first".asLeft[Int] |+| "second".asLeft[Int] |+| 1.asRight[String]
```

### `combineN`

Combine value with itself `n` times.

```scala
def combineN[A](a: A, n: Int): A
```
Usage:

```scala mdoc:reset
import cats.Semigroup
import cats.instances.int._
import cats.instances.string._
import cats.instances.list._
import cats.instances.option._

Semigroup[List[Int]].combineN(List(1, 2), 5)
Semigroup[Option[String]].combineN(Option("hi"), 5)
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia/blob/master/AbstractAlgebra.MD#semigroup