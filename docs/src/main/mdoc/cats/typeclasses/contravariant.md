---
id: contravariant
title: Contravariant
---

`Contravariant functor` allows transformation from one value to another using reversed function and preserving the structure.
Is used on structures that define some kind of operation. 

## Definition

```scala
import cats.Invariant

trait Contravariant[F[_]] extends Invariant[F] {
  def contramap[A, B](fa: F[A])(f: B ⇒ A): F[B]
}
```

## Methods

Transform value using reversed function and preserving the structure.

### `contramap`

```scala
def contramap[F[_], A, B](fa: F[A])(f: B => A): F[B]
```

Usage:

Suppose we want to be able to convert values of different types to `String`.

1) Create trait representing convert operation:
```scala mdoc
trait ToStringConverter[A] {
  def toStr(v: A): String
}
``` 

2) Define `Contravariant[ToStringConverter]`:
```scala mdoc
import cats.Contravariant

object instances {
  implicit object ToStringConverterContravariant extends Contravariant[ToStringConverter] {
    def contramap[A, B](fa: ToStringConverter[A])(f: B => A): ToStringConverter[B] = new ToStringConverter[B] {
      def toStr(v: B): String = fa.toStr(f(v))
    }
  }
}
```

3) Define case class to be used in example:
```scala mdoc
case class Box(v: Int)
```

4) Finally, example:
```scala mdoc
// basic converter from string to string
val stringConverter: ToStringConverter[String] = identity[String]

import instances.ToStringConverterContravariant
import cats.syntax.contravariant._

// box converter is created from string converter
val boxConverter: ToStringConverter[Box] = stringConverter.contramap(b => s"Box[${b.v}]")

// convertion
boxConverter.toStr(Box(1))
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#contravariant-contravariant-functor