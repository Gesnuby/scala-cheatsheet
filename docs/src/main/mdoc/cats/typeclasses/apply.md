---
id: apply
title: Apply
---

Allows to apply function already inside the context to the arguments inside the context.

## Definition

```scala
import cats.Functor
import cats.Semigroupal

trait Apply[F[_]] extends Functor[F] with Semigroupal[F] {
  def ap[A, B](ff: F[A ⇒ B])(fa: F[A]): F[B]
}
```

## Methods

### `ap` (`<*>`)

Applies function inside context to the value.

```scala
def ap[F[_], A, B](ff: F[A => B])(fa: F[A]): F[B]
```

Usage:

```scala mdoc:reset
import cats.Apply
import cats.instances.list._

val f1 = (x: Int) => x * x
val f2 = (x: Int) => x + 10
Apply[List].ap(List(f1, f2))(List(1, 2, 3))

// using syntax
import cats.syntax.apply._
import cats.syntax.either._
import cats.instances.either._

val f3 = (x: String) => x.toUpperCase
f3.asRight[String] <*> "hello".asRight[String]
f3.asRight[String] <*> "world".asLeft[String]
```

### `productR` (`*>`)

Compose two contexts discarding value from the first.

```scala
def productR[F[_], A, B](fa: F[A])(fb: F[B]): F[B]
```

Usage:

```scala mdoc:reset
import cats.Apply
import cats.instances.list._

Apply[List].productR(List(1, 2, 3))(List(10, 20))

// using syntax
import cats.syntax.apply._
import cats.syntax.either._
import cats.instances.either._

1.asRight[String] *> 100.asRight[String]
"error".asLeft[Int] *> 100.asRight[String]
```

### `productL` (`<*`)

Compose two contexts discarding value from the second.

```scala
def product[F[_], A, B](fa: F[A])(fb: F[B]): F[A]
```

Usage:

```scala mdoc:reset
import cats.Apply
import cats.instances.list._

Apply[List].productL(List(1, 2, 3))(List(10, 20))

// using syntax
import cats.syntax.apply._
import cats.syntax.either._
import cats.instances.either._

1.asRight[String] <* 100.asRight[String]
1.asRight[String] <* "error".asLeft[Int]
```

### `map2`

Binary version of `Functor#map`

```scala
def map2[F[_], A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C]
```

Usage:

```scala mdoc:reset
import cats.Apply
import cats.instances.option._

val f = (x: Int, y: String) => x.toInt + y
Apply[Option].map2(Option(47), Option("deg"))(f)
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#apply