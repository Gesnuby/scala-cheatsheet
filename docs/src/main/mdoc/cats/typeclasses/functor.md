---
id: functor
title: Functor (Covariant)
---

Provides ability to transform content without changing the structure.

## Definition

```scala
trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A ⇒ B): F[B]
}
```

## Methods

### `map`

Applies function preserving the structure.

```scala
def map[A, B](fa: F[A])(f: A => B): F[B]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.option._

Functor[Option].map(Option(12))(_ * 2)

// using syntax
import cats.syntax.functor._
import cats.instances.list._

// fmap is same as map
List(1, 2, 3).fmap(_ + 1)

```

### `lift`

Lifts function to operate on functors.

```scala
def lift[F[_], A, B](f: A => B): F[A] => F[B]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.option._
import cats.instances.list._

val f1 = Functor[Option].lift((x: Int) => x * x)
f1(Option(6))

val f2 = Functor[List].lift((s: String) => s.reverse)
f2(List("hello", "world"))
```

### `void`

Empties value preserving the structure.

```scala
def void[F[_], A](fa: F[A]): F[Unit]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.option._

Functor[Option].void(Option(12))

// using syntax
import cats.syntax.functor._
import cats.instances.list._

List(1, 2, 3).void
```

### `fproduct`

Tuples value with result of applying the function.

```scala
def fproduct[F[_], A, B](fa: F[A])(f: A => B): F[(A, B)]
```

Usage:

```scala mdoc:reset
import cats.instances.list._
import cats.Functor

Functor[List].fproduct(List(1, 2, 3))(_ + 100)

// using syntax
import cats.syntax.functor._
import cats.instances.option._

Option(9).fproduct(_ * 9)
```

### `as`

Replaces value inside structure.

```scala
def as[F[_], A, B](fa: F[A], b: B): F[B]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.list._

Functor[List].as(List(1, 2, 3), "hi")

// using syntax
import cats.instances.either._
import cats.syntax.either._
import cats.syntax.functor._

4.asRight[String].as(104)
"error".asLeft[Int].as(104)
```

### `tupleLeft`

Tuples value with the supplied value on the left.

```scala
def tupleLeft[F[_], A, B](fa: F[A], b: B): F[(B, A)]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.list._

Functor[List].tupleLeft(List(1, 2, 3), 100)

// using syntax
import cats.instances.either._
import cats.syntax.either._
import cats.syntax.functor._

4.asRight[String].tupleLeft("hello")
4.asLeft[Int].tupleLeft("hello")
```

### `tupleRight`

Tuples value with the supplied value on the right.

```scala
def tupleRight[F[_], A, B](fa: F[A], b: B): F[(A, B)]
```

Usage:

```scala mdoc:reset
import cats.Functor
import cats.instances.list._

Functor[List].tupleLeft(List(1, 2, 3), 100)

// using syntax
import cats.instances.either._
import cats.syntax.either._
import cats.syntax.functor._

4.asRight[String].tupleRight("hello")
4.asLeft[Int].tupleRight("hello")
```

### `compose`

Allows composing functors.

Usage:

```scala mdoc:reset
import cats.Functor
import cats.syntax.either._
import cats.instances.either._
import cats.instances.list._

val composed = Functor[Either[String, ?]].compose(Functor[List])
val value = List("hello", "world").asRight[String]
composed.map(value)(_.toUpperCase)
```
## Links

1) https://github.com/lemastero/scala_typeclassopedia#functor-covariant-functor