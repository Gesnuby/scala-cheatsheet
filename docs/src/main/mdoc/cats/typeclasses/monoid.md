---
id: monoid
title: Monoid
---

`Monoid` is a `Semigroup` with an identity element. 
Identity element does not affect the result of binary operation.

## Definition

```scala
import cats.kernel.Semigroup

trait Monoid[A] extends Semigroup[A] {
  def empty: A
}
```

## Methods

### `empty`

Return identity element.

```scala
def empty[A]: A    
```

Usage:

```scala mdoc:reset
import cats.Monoid
import cats.instances.int._
import cats.instances.option._
import cats.instances.list._

Monoid[Int].empty
Monoid[Option[Int]].empty
Monoid[List[Int]].empty

import cats.syntax.semigroup._
5 |+| Monoid[Int].empty
```

### `isEmpty`

Check if element is an identity.

```scala
def isEmpty[A]: Boolean
```

Usage:

```scala mdoc:reset
import cats.instances.int._
import cats.instances.list._
import cats.syntax.monoid._

0.isEmpty
1.isEmpty
List().isEmpty
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia/blob/master/AbstractAlgebra.MD#monoid