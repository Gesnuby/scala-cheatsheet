---
id: semigroupal
title: Semigroupal
---

Allows composing independent effectful values.

## Definition

```scala
trait Semigroupal[F[_]] {
  def product[A, B](fa: F[A], fb: F[B]): F[(A, B)] 
}
```

## Methods

### `product`

Combines `F[A]` and `F[B]` into `F[(A, B)]` maintaining effects of both contexts.

```scala
def product[F[_], A, B](fa: F[A], F[B]): F[(A, B)]
```

Usage:

```scala mdoc:reset
import cats.Semigroupal
import cats.instances.option._

Semigroupal[Option].product(Option(1), Option(2))
Semigroupal[Option].product(None, Option(2))

// using syntax
import cats.syntax.semigroupal._
import cats.instances.either._
import cats.syntax.either._

10.asRight[String] product "error".asLeft[Int]
10.asRight[String] product 20.asRight[String]
```