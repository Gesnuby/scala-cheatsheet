---
id: applicative
title: Applicative
---

Functor that can add value to the container.

## Definition

```scala
import cats.Apply

trait Applicative[F[_]] extends Apply[F] {
  def pure[A](x: A): F[A]
}
```

## Methods

### `pure`

Lifts value to the container.

```scala
def pure[F[_], A](v: A): F[A]
```

Usage:

```scala mdoc:reset
import cats.Applicative
import cats.instances.option._

Applicative[Option].pure(10)

// using syntax
import cats.syntax.applicative._
import cats.instances.either._

10.pure[Either[String, ?]]
```

### `unit`

Returns `F[Unit]`. Is equivalent to `pure(())`.

```scala
def unit[F[_]]: F[Unit]
```

Usage:

```scala mdoc:reset
import cats.Applicative
import cats.instances.option._

Applicative[Option].unit
```

### `replicateA`

Replicates `F[A]` `n` times to produce `F[List[A]]`.

```scala
def replicateA[F[_], A](fa: F[A], n: Int): F[List[A]]
```

Usage:

```scala mdoc:reset
import cats.Applicative
import cats.instances.option._

Applicative[Option].replicateA(3, Option(10))
Applicative[Option].replicateA(2, None)

// using syntax
import cats.syntax.applicative._
import cats.instances.either._
import cats.syntax.either._

10.asRight[String].replicateA(3)
"error".asLeft[Int].replicateA(5)
```

## Links

1) https://github.com/lemastero/scala_typeclassopedia#applicative-applicative-functor