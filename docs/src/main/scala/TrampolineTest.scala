import scala.annotation.tailrec

object TrampolineTest {

  def main(args: Array[String]): Unit = {
    println(factorial(10000))
  }

  def factorial(n: Int): BigInt = {
    import Trampoline._

    def loop(i: Int): Trampoline[BigInt] = {
      if (i == 0) done(1)
      else more(loop(i - 1)).flatMap(r => done(r * i))
    }
    loop(n).runT
  }

  object Trampoline {
    def done[A](v: A): Trampoline[A] = Done(v)
    def more[A](t: => Trampoline[A]): Trampoline[A] = More(() => t)
  }

  sealed trait Trampoline[A] {

    def flatMap[B](cont: A => Trampoline[B]): Trampoline[B] = this match {
      case fm: FlatMap[a, _] =>
        FlatMap(fm.tramp, { r: a =>
          fm.cont(r).flatMap(cont)
        })
      case t => FlatMap(t, cont)
    }

    @tailrec
    final def runT: A = this match {
      case Done(v) => v
      case More(tramp) => tramp().runT
      case FlatMap(tramp, cont) => tramp match {
        case Done(v) => cont(v).runT
        case More(innerTramp) => innerTramp().flatMap(cont).runT
        case FlatMap(innerTramp, innerCont) => innerTramp.flatMap { r =>
          innerCont(r).flatMap(cont)
        }.runT
      }
    }
  }
  final case class More[A](tramp: () => Trampoline[A]) extends Trampoline[A]
  final case class Done[A](v: A) extends Trampoline[A]
  final case class FlatMap[A, B](tramp: Trampoline[A], cont: A => Trampoline[B]) extends Trampoline[B]
}
