const siteConfig = {
  title: 'Scala CheatSheet',
  tagline: 'A website for docs for various scala libraries',
  url: 'https://your-docusaurus-test-site.com', // Your website URL
  baseUrl: '/', // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  projectName: 'scala-cheatsheet',
  organizationName: 'gesnuby',
  customDocsPath: 'docs/target/mdoc',
  headerLinks: [
    {doc: 'cats/typeclasses/semigroup', label: 'Cats'},
    {doc: 'cats-effect/typeclasses/todo', label: 'Cats Effect'}
  ],
  headerIcon: 'img/docusaurus.svg',
  favicon: 'img/favicon.png',
  colors: {
    primaryColor: '#122932',
    secondaryColor: '#c61d13',
  },
  copyright: `Copyright © ${new Date().getFullYear()} Your Name or Your Company Name`,
  highlight: {
    theme: 'default',
  },
  scripts: ['https://buttons.github.io/buttons.js'],
  onPageNav: 'separate',
  cleanUrl: true,
  enableUpdateTime: true,
  docsSideNavCollapsible: true
};

module.exports = siteConfig;
