name := "scala-cheatsheet"

version := "0.1"

scalaVersion := "2.12.8"

lazy val `scala-cheatsheet` = project
  .in(file("."))
  .settings(
    moduleName := "scala-cheatsheet",
    name := moduleName.value,
    scalaSettings
  )

lazy val docs = project
  .in(file("docs"))
  .settings(
    moduleName := "scala-cheatsheet-docs",
    name := moduleName.value,
    mdocIn := (sourceDirectory in Compile).value / "mdoc",
    scalaSettings,
    dependencySettings,
    addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.8")
  )
  .dependsOn(`scala-cheatsheet`)
  .enablePlugins(MdocPlugin, DocusaurusPlugin)

lazy val scalaSettings = Seq(
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-feature",
    "-language:higherKinds",
    "-language:postfixOps",
    "-Xfatal-warnings",
    "-Ypartial-unification"
  )
)

lazy val dependencySettings = Seq(
  libraryDependencies ++= Seq(
    "org.typelevel" %% "cats-core" % "1.5.0"
  )
)